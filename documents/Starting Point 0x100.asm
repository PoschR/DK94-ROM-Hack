Warning: RGBASM could not handle HALT instruction properly (0x00000186)
SECTION "rom0", HOME[0]

	INCBIN "Donkey Kong.gb",$0,$100-$0
	NOP
	JP jmp_150

	INCBIN "Donkey Kong.gb",$104,$150-$104
jmp_150:
	DI
	LD SP,$FFFE
	LD A,$01
	RST $10
	LD A,$5D
	CALL sub_388e
	CALL sub_226
	CALL sub_fc1
	LD A,$03
	CALL sub_1e4f
	CALL sub_3737
	CALL sub_193
	LD A,$07
	LD [$C0A3],A
	LDH [$FF],A
	EI
jmp_175:			;main loop?
	CALL sub_1022	;reads joy pad input and stores the difference into 0xFF8B, the actual pressed buttons into 0xFF8A
	CALL sub_1ec	;reset routine
	CALL sub_1a9	;jumps to code in the memory banks
	LD HL,$C72F
	RES 0,[HL]
	LD HL,$C0A0
	HALT
jmp_187:			;wait for vblank
	BIT 0,[HL]
	JR Z,jmp_187
	RES 0,[HL]  
	LD HL,$FF95
	INC [HL]
	JR jmp_175
sub_193:			;set some IO flags
	LD A,$40
	LDH [$41],A
	XOR A
	LD [$DEFB],A
	LDH [$05],A
	LD A,$BA
	LDH [$06],A
	XOR A
	LDH [$07],A
	LD A,$04
	LDH [$07],A
	RET

sub_1a9:
	LDH A,[$8E]
	RST $08			;skip code?	
	LD [HLI],A
	DEC E
	LD [HLI],A
	DEC E
	LD C,$34
	SBC A,D
	DEC [HL]
	XOR B
	JR Z,jmp_1d9
	DEC E
	LD [HLI],A
	DEC E
	LD [HLI],A
	DEC E
	LD D,A
	LD [HL],$22
	DEC E
	LD [HLI],A
	DEC E
	LD [HLI],A
	DEC E
	CP $C1
	LD [HLI],A
	DEC E
	LD [HLI],A
	DEC E
	LD [HLI],A
	DEC E
	CP L
	LD B,$DF
	INC [HL]
	SUB D
	LD B,$22
	DEC E
	LD L,H
	LD H,$82
	DEC [HL]
	LD H,E
jmp_1d9:
	LD [HL],$22
	DEC E
	NOP
	LD B,B
	LD [HLI],A
	DEC E
	LD A,C
	LD [HL],$22
	DEC E
	LD [HL],C
	INC [HL]
	LD [HLI],A
	DEC E
	LD A,[BC]
	ADD HL,HL
	LD [HLI],A
	DEC E
sub_1ec:
	LDH A,[$8A]
	AND $0F
	CP $0F
	RET NZ			;return if A+B+ST+SE are pressed
	LDH A,[$8E]
	CP $03
	RET C
	CP $1A
	RET Z
	LD A,$5F
	CALL sub_3891
	CALL sub_ff0
	LD A,$03
	CALL sub_1e4f
	CALL sub_10ab
	LDH A,[$FF]
	AND $FA
	LDH [$FF],A
	EI
	CALL sub_226
	LD A,$07
	LD [$C0A3],A
	LDH [$FF],A
	XOR A
	LDH [$90],A
	LD A,$02
	LDH [$8E],A
	JP jmp_ffd
sub_226:
	LD A,[$C82B]
	PUSH AF
	LD A,[$DA40]
	PUSH AF
	LD HL,$C000
	LD BC,$1EF7
	XOR A
	CALL sub_1057	;set a chunk at address HL and size BC to A  
	LD HL,$DF00
	LD BC,$0100
	XOR A
	CALL sub_1057
	LD HL,$FF8A
	LD C,$20
	XOR A
	CALL sub_1060	;set a chunk at adress HL and sice C to A
	POP AF
	LD [$DA40],A
	POP AF
	AND $CD
	LD [$C82B],A
	LD A,$03
	RST $10
	CALL sub_4000  ;initializes stuff in the RAM with default values
	CALL sub_1d11
	LD A,$E7
	LD [$C0A2],A
	LD A,$01
	LD [$C851],A
	LD A,$04
	LD [$DA43],A
	LD HL,$C859
	XOR A
	LD [HLD],A
	DEC A
	LD [HL],A
	RET


	INCBIN "Donkey Kong.gb",$275,$fc1-$275
sub_fc1:
	LD C,$80
	LD B,$0A
	LD HL,$0FCF
jmp_fc8:
	LD A,[HLI]
	LD [C],A
	INC C
	DEC B
	JR NZ,jmp_fc8
	RET


	INCBIN "Donkey Kong.gb",$fcf,$ff0-$fcf
sub_ff0:
	DI
	LDH A,[$FF]
	LD [$C0A3],A
	AND $FE
	LDH [$FF],A
	EI
	JR jmp_1010
jmp_ffd:			;Restores original interrupts and LCD control
	LD A,[$C0A3]
	LDH [$FF],A
	LD A,[$C0A2]
	LDH [$40],A
	RET


	INCBIN "Donkey Kong.gb",$1008,$1010-$1008
jmp_1010:
	LDH A,[$44]
	CP $91
	JR C,jmp_1010
	LDH A,[$40]
	AND $7F
	LDH [$40],A
	LD A,[$C0A3]
	LDH [$FF],A
	RET

sub_1022:			
	LD A,$20
	LDH [$00],A
	LDH A,[$00]
	LDH A,[$00]
	CPL
	AND $0F
	SWAP A
	LD B,A
	LD A,$30
	LDH [$00],A
	LD A,$10
	LDH [$00],A
	LDH A,[$00]
	LDH A,[$00]
	LDH A,[$00]
	LDH A,[$00]
	LDH A,[$00]
	LDH A,[$00]		;read joypad
	CPL
	AND $0F
	OR B
	LD C,A			;complete joypad info in C now
	LDH A,[$8A]		;previous joypad input?
	XOR C			;difference
	AND C
	LDH [$8B],A
	LD A,C
	LDH [$8A],A
	LD A,$30
	LDH [$00],A
	RET

jmp_1057:
sub_1057:
	LD [HLI],A
	LD D,A
	DEC BC
	LD A,B
	OR C
	LD A,D
	JR NZ,jmp_1057
	RET

jmp_1060:
sub_1060:
	LD [HLI],A
	DEC C
	JR NZ,jmp_1060
	RET

jmp_1065:
sub_1065:
	LD A,[HLI]
	LD [DE],A
	INC DE
	DEC BC
	LD A,B
	OR C
	JR NZ,jmp_1065
	RET

jmp_106e:
sub_106e:
	LD A,[HLI]
	LD [DE],A
	INC DE
	DEC C
	JR NZ,jmp_106e
	RET


	INCBIN "Donkey Kong.gb",$1075,$10ab-$1075
sub_10ab:
	LD HL,$9800
	LD BC,$0800
	JR jmp_10be

	INCBIN "Donkey Kong.gb",$10b3,$10be-$10b3
jmp_10be:
	XOR A
	JP jmp_1057

	INCBIN "Donkey Kong.gb",$10c2,$1d07-$10c2
sub_1d07:
	LD HL,$DA5F
	LD DE,$DA61
	LD A,E
	LD [HLI],A
	LD [HL],D
	RET

sub_1d11:
	CALL sub_1d07
	LD HL,$DA71
	LD DE,$DA61
	LD A,E
	LD [HLI],A
	LD A,D
	LD [HLI],A
	LD A,E
	LD [HLI],A
	LD [HL],D
	RET


	INCBIN "Donkey Kong.gb",$1d22,$1d2a-$1d22
jmp_1d2a:
sub_1d2a:
	LD HL,$FF90
	INC [HL]
	RET


	INCBIN "Donkey Kong.gb",$1d2f,$1dfd-$1d2f
sub_1dfd:
	LD HL,$7918
	LD D,$00
	ADD A,A
	LD E,A
	ADD HL,DE
	LDH A,[$8C]
	PUSH AF
	LD A,$1B
	RST $10
	LD A,[HLI]
	LD H,[HL]
	LD L,A
	LD A,[HLI]
	LD C,A
jmp_1e10:
	LD A,[HLI]
	PUSH HL
	LD H,$00
	ADD A,A
	ADD A,A
	ADD A,A
	LD L,A
	ADD HL,HL
	ADD HL,HL
	LD DE,$C200
	ADD HL,DE
	POP DE
	LD A,[DE]
	INC DE
	LD [HLI],A
	LD A,[DE]
	INC DE
	LD [HLI],A
	LD A,[DE]
	INC DE
	LD [HLI],A
	LD A,[DE]
	INC DE
	LD [HLI],A
	XOR A
	LD [HLI],A
	LD [HLI],A
	LD [HLI],A
	LD A,[DE]
	INC DE
	LD [HLI],A
	XOR A
	LD [HLI],A
	LD [HLI],A
	LD [HL],A
	LD L,E
	LD H,D
	DEC C
	JR NZ,jmp_1e10
	POP AF
	RST $10
	RET


	INCBIN "Donkey Kong.gb",$1e3e,$1e4f-$1e3e
sub_1e4f:
	LD B,A
	ADD A,A
	ADD A,B
	LD B,$00
	LD C,A
	LD HL,$1E86
	ADD HL,BC
	LD BC,$DA3D
	LD A,[HLI]
	LDH [$47],A
	LD [BC],A
	INC BC
	LD A,[HLI]
	LDH [$48],A
	LD [BC],A
	INC BC
	LD A,[HL]
	LDH [$49],A
	LD [BC],A
	RET


	INCBIN "Donkey Kong.gb",$1e6b,$1ebf-$1e6b
sub_1ebf:
	RET

sub_1ec0:
	LD A,$0A
	LD [$C86A],A
	LD [$0000],A
	RET

sub_1ec9:
	LD A,$00
	LD [$C86A],A
	LD [$0000],A
	RET


	INCBIN "Donkey Kong.gb",$1ed2,$257f-$1ed2
sub_257f:
	PUSH HL
	LD HL,$C809
	LD A,C
	LD [HLI],A
	LD A,B
	LD [HLI],A
	LD A,E
	LD [HLI],A
	LD [HL],D
	LD DE,$0000
	POP HL
jmp_258e:
	LD A,[HLI]
	LD B,A
	LD C,$08
jmp_2592:
	PUSH BC
	BIT 0,B
	JR Z,jmp_25a9
	LD A,[HLI]
	CALL sub_25e7
	LD A,[$C809]
	CP E
	JR NZ,jmp_25dd
	LD A,[$C80A]
	CP D
	JR NZ,jmp_25dd
	JR jmp_25e5
jmp_25a9:
	LD A,[HLI]
	LD C,A
	LD A,[HLI]
	PUSH HL
	LD B,A
	AND $F0
	SWAP A
	LD H,A
	LD A,E
	SUB C
	LD L,A
	LD A,D
	SBC A,H
	LD H,A
	LD A,B
	AND $0F
	ADD A,$03
	LD C,A
	LD A,[$C80B]
	ADD A,L
	LD L,A
	LD A,[$C80C]
	ADC A,H
	LD H,A
jmp_25c9:
	LD A,[HLI]
	CALL sub_25e7
	DEC C
	JR NZ,jmp_25c9
	POP HL
	LD A,[$C809]
	CP E
	JR NZ,jmp_25dd
	LD A,[$C80A]
	CP D
	JR Z,jmp_25e5
jmp_25dd:
	POP BC
	SRL B
	DEC C
	JR NZ,jmp_2592
	JR jmp_258e
jmp_25e5:
	POP BC
	RET

sub_25e7:
	LD B,A
	PUSH HL
	LD HL,$C80B
	LD A,[HLI]
	LD H,[HL]
	LD L,A
	ADD HL,DE
	LD [HL],B
	INC DE
	POP HL
	RET


	INCBIN "Donkey Kong.gb",$25f4,$338f-$25f4
sub_338f:
	LD DE,$370F
	ADD HL,DE
	LD A,L
	AND $7F
	SLA L
	RL H
	ADD A,A
	LD E,$08
	ADD A,E
	LD E,A
	LD A,H
	ADD A,A
	ADD A,A
	ADD A,A
	ADD A,$10
	LD D,A
	RET


	INCBIN "Donkey Kong.gb",$33a7,$3737-$33a7
sub_3737:
	LDH A,[$FF]
	SET 0,A
	EI
	LD A,$0A
	CALL sub_388e
	LDH A,[$00]
	CP $FF
	JR NZ,jmp_3761
	CALL sub_38f4
	LDH A,[$00]
	CP $FF
	JR NZ,jmp_3761
	LD A,$09
	CALL sub_388e
	LD HL,$C82B
	RES 7,[HL]
	LD A,$CC
	LD [$DA40],A
	DI
	RET

jmp_3761:
	LD HL,$C82B
	SET 7,[HL]
	LD A,$9C
	LD [$DA40],A
	LD A,$2D
	CALL sub_3857
	LD A,$1F
	CALL sub_3857
	DI
	CALL sub_386a
	LD A,$E4
	LDH [$47],A
	LD HL,$9800
	LD DE,$000C
	LD A,$80
	LD B,$0D
jmp_3787:
	LD C,$14
jmp_3789:
	LD [HLI],A
	INC A
	DEC C
	JR NZ,jmp_3789
	ADD HL,DE
	DEC B
	JR NZ,jmp_3787
	LD A,$1E
	RST $10
	LD HL,$5212
	LD DE,$8800
	LD BC,$1000
	CALL sub_257f
	LD A,$81
	LDH [$40],A
	LD A,$27
	CALL sub_3857
	EI
	DI
	CALL sub_386a
	LD HL,$5D9C
	LD DE,$8800
	LD BC,$06E0
	CALL sub_257f
	LD A,$81
	LDH [$40],A
	LD A,$2A
	CALL sub_3857
	EI
	DI
	CALL sub_386a
	LD HL,$6145
	LD DE,$8800
	LD BC,$0860
	CALL sub_257f
	LD A,$81
	LDH [$40],A
	LD A,$28
	CALL sub_3857
	EI
	DI
	CALL sub_386a
	LD HL,$4710
	LD DE,$8800
	LD BC,$1000
	CALL sub_257f
	LD A,$81
	LDH [$40],A
	LD A,$10
	CALL sub_3857
	EI
	DI
	CALL sub_386a
	LD HL,$4C92
	LD DE,$8800
	LD BC,$0FD2
	CALL sub_257f
	LD A,$81
	LDH [$40],A
	LD A,$11
	CALL sub_3857
	EI
	DI
	CALL sub_386a
	LD A,$0C
	RST $10
	LD HL,$5DDD
	LD DE,$8800
	LD BC,$0680
	CALL sub_1065
	LD A,$81
	LDH [$40],A
	LD A,$20
	CALL sub_3857
	EI
	DI
	CALL sub_386a
	LD HL,$6438
	LD DE,$8800
	LD BC,$0BCA
	CALL sub_1065
	LD A,$81
	LDH [$40],A
	LD A,$20
	CALL sub_3857
	EI
	LD A,$05
	CALL sub_3891
	LD A,$09
	CALL sub_3891
	JP jmp_391b
sub_3857:
	CALL sub_3891
	LD B,$08
	JR jmp_3860
sub_385e:
	LD B,$04
jmp_3860:
	LD DE,$06D6
	CALL sub_3885
	DEC B
	JR NZ,jmp_3860
	RET

sub_386a:
	LDH A,[$FF]
	LD [$C0A3],A
	RES 0,A
	LDH [$FF],A
jmp_3873:
	LDH A,[$44]
	CP $91
	JR C,jmp_3873
	LDH A,[$40]
	AND $7F
	LDH [$40],A
	LD A,[$C0A3]
	LDH [$FF],A
	RET

jmp_3885:
sub_3885:
	NOP
	NOP
	NOP
	DEC DE
	LD A,D
	OR E
	JR NZ,jmp_3885
	RET

sub_388e:
	LD B,A
	JR jmp_3898
sub_3891:
	LD B,A
	LD A,[$C82B]
	BIT 7,A
	RET Z
jmp_3898:
	LDH A,[$8C]
	PUSH AF
	LD A,$1E
	RST $10
	LD HL,$4000
	LD A,B
	ADD A,A
	LD D,$00
	LD E,A
	ADD HL,DE
	LD A,[HLI]
	LD H,[HL]
	LD L,A
	CALL sub_38bf
	CALL sub_385e
	POP AF
	RST $10
	RET


	INCBIN "Donkey Kong.gb",$38b3,$38bf-$38b3
sub_38bf:			;maybe map loading? 
	LD A,[HL]
	AND $07
	LD B,A
	LD C,$00
	PUSH BC
jmp_38c6:
	XOR A
	LD [C],A
	LD A,$30
	LD [C],A
	LD B,$10
jmp_38cd:
	LD E,$08
	LD A,[HLI]
	LD D,A
jmp_38d1:
	BIT 0,D
	LD A,$10
	JR NZ,jmp_38d9
	LD A,$20
jmp_38d9:
	LD [C],A
	LD A,$30
	LD [C],A
	RR D
	DEC E
	JR NZ,jmp_38d1
	DEC B
	JR NZ,jmp_38cd
	LD A,$20
	LD [C],A
	LD A,$30
	LD [C],A
	POP BC
	DEC B
	RET Z
	PUSH BC
	CALL sub_385e
	JR jmp_38c6
sub_38f4:
	LD A,$20
	LDH [$00],A
	LDH A,[$00]
	LDH A,[$00]
	CPL
	AND $0F
	SWAP A
	LD B,A
	LD A,$30
	LDH [$00],A
	LD A,$10
	LDH [$00],A
	LDH A,[$00]
	LDH A,[$00]
	LDH A,[$00]
	LDH A,[$00]
	LDH A,[$00]
	LDH A,[$00]
	LD A,$30
	LDH [$00],A
	RET

jmp_391b:
	LD C,$08
	LD A,$68
jmp_391f:
	PUSH AF
	PUSH BC
	CALL sub_3891
	POP BC
	POP AF
	INC A
	DEC C
	JR NZ,jmp_391f
	RET


	INCBIN "Donkey Kong.gb",$392b,$4000-$392b

SECTION "bank1",DATA,BANK[$1]

	INCBIN "Donkey Kong.gb",$4000,$4000-$4000
sub_4000:
	LDH A,[$90]
	RST $08
	DEC C
	LD B,B
	LD L,H
	LD B,B
	RST $08
	LD B,C
	DEC DE
	LD B,C
	LD B,D
	LD B,C
	CALL sub_1ec0
	XOR A
	CALL sub_1ebf
	LD HL,$BBC0
	PUSH HL
	LD C,$0C
	XOR A
	LD [$C706],A
	CALL sub_1060
	LD HL,$C220
	POP DE
	LD C,$06
jmp_4027:
	PUSH BC
	PUSH DE
	LD A,[HL]
	OR A
	JR Z,jmp_4050
	PUSH HL
	PUSH DE
	LD HL,$C2E0
	LD DE,$0020
	LD B,$07
jmp_4037:
	LD A,[HL]
	OR A
	JR Z,jmp_403f
	ADD HL,DE
	INC B
	JR jmp_4037
jmp_403f:
	POP DE
	LD A,B
	LD [DE],A
	INC DE
	SWAP A
	LD [DE],A
	LD D,H
	LD E,L
	POP HL
	PUSH HL
	LD C,$20
	CALL sub_106e
	POP HL
jmp_4050:
	LD BC,$0020
	ADD HL,BC
	POP DE
	INC DE
	INC DE
	POP BC
	DEC C
	JR NZ,jmp_4027
	CALL sub_1ec9
	LD HL,$C707
	LD DE,$C709
	LD A,[HLI]
	LD [DE],A
	INC DE
	LD A,[HL]
	LD [DE],A
	CALL sub_1d2a
	LD A,$48
	LD [$DA3D],A
	LD [$DA3E],A
	XOR A
	LDH [$95],A
	LD A,$20
	LD [$C6FE],A
	LD A,$03
	CALL sub_1dfd
	LD A,[$C70C]
	ADD A,A
	ADD A,A
	ADD A,A
	LD D,$00
	LD E,A
	LD HL,$DB18
	ADD HL,DE
	ADD HL,DE
	LD E,[HL]
	PUSH HL
	PUSH HL
	LD HL,$6114
	ADD HL,DE
	ADD HL,DE
	LD A,[HLI]
	LD H,[HL]
	LD L,A
	LD DE,$0008
	ADD HL,DE
	LD A,[HLI]
	DEC A
	SWAP A
	SRL A
	LD C,A
	LD B,[HL]
	DEC B
	SWAP B
	SRL B
	POP HL
	INC HL
	LD A,[HLI]
	LD H,[HL]
	LD L,A
	CALL sub_338f
	LD A,D
	LD [$C2C3],A
	LD A,E
	LD [$C2C7],A
	POP HL
	DEC HL
	DEC HL
	LD A,[HL]
	CP $F4
	JR NZ,jmp_40c5
	LD C,$08
jmp_40c5:
	CP $A8
	JR NZ,jmp_40ce
	LD DE,$000E
	ADD HL,DE
	LD A,[HL]
jmp_40ce:
	LD [$C2A1],A
	LD [$C2C1],A
	XOR A
	LD [$C2A2],A
	INC A
	LD [$C2A0],A
	LD [$C2C0],A
	LD [$C2C2],A
	LDH A,[$42]
	ADD A,$50
	AND $F8
	LD [$C2A3],A
	LD [$C223],A
	LD [$C243],A
	ADD A,C
	LD [$C263],A
	LD [$C283],A
	LDH A,[$43]
	ADD A,$50
	AND $F8
	LD [$C2A7],A
	LD [$C227],A
	LD [$C267],A
	ADD A,B
	LD [$C247],A
	LD [$C287],A
	CALL sub_4370
	CALL sub_43a6
	XOR A
	LD [$C22B],A
	JP jmp_1d2a

	INCBIN "Donkey Kong.gb",$411b,$4370-$411b
sub_4370:
	LD DE,$001C
	LD HL,$C223
	LD A,[HL]
	SUB $50
	LD [HLI],A
	INC HL
	INC HL
	INC HL
	LD A,[HL]
	SUB $50
	LD [HL],A
	ADD HL,DE
	LD A,[HL]
	SUB $50
	LD [HLI],A
	INC HL
	INC HL
	INC HL
	LD A,[HL]
	ADD A,$50
	LD [HL],A
	ADD HL,DE
	LD A,[HL]
	ADD A,$50
	LD [HLI],A
	INC HL
	INC HL
	INC HL
	LD A,[HL]
	SUB $50
	LD [HL],A
	ADD HL,DE
	LD A,[HL]
	ADD A,$50
	LD [HLI],A
	INC HL
	INC HL
	INC HL
	LD A,[HL]
	ADD A,$50
	LD [HL],A
	RET

sub_43a6:
	LD B,$00
	LD DE,$C707
	LD A,[DE]
	INC DE
	LD C,A
	LD HL,$C2C3
	LD A,[HL]
	PUSH AF
	SUB $06
	LD [HLI],A
	POP AF
	INC HL
	SUB C
	SUB $50
	CPL
	INC A
	SRA A
	RR B
	SRA A
	RR B
	SRA A
	RR B
	SRA A
	RR B
	SRA A
	RR B
	LD [HLI],A
	LD A,B
	LD [HLI],A
	LD B,$00
	LD A,[DE]
	LD C,A
	LD A,[HL]
	DEC [HL]
	DEC [HL]
	INC HL
	INC HL
	SUB C
	SUB $50
	CPL
	INC A
	SRA A
	RR B
	SRA A
	RR B
	SRA A
	RR B
	SRA A
	RR B
	SRA A
	RR B
	LD [HLI],A
	LD [HL],B
	RET


	INCBIN "Donkey Kong.gb",$43f9,$8000-$43f9
