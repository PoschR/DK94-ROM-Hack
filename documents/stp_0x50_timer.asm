#org 150
di
ld sp,FFFE
ld a,01
rst 10
ld a,5D
call Label388E
call Label226
call LabelFC1
ld a,03
call Label1E4F
call Label3737
call Label193
ld a,07
ld (C0A3),a
ld (FF00+FF),a
ei
Label175:
call Label1022
call Label1EC
call Label1A9
ld hl,C72F
res 0,(hl)
ld hl,C0A0
halt
Label187:
bit 0,(hl)
jr z,Label187
res 0,(hl)
ld hl,FF95
inc (hl)
jr Label175
Label193:
ld a,40
ld (FF00+41),a
xor a,a
ld (DEFB),a
ld (FF00+05),a
ld a,BA
ld (FF00+06),a
xor a,a
ld (FF00+07),a
ld a,04
ld (FF00+07),a
ret

#org 10
Label10:
ld (FF00+8C),a
ld (2000),a
ret

#org 388E
Label388E:
ld b,a
jr Label3898
Label3891:
ld b,a
ld a,(C82B)
bit 7,a
ret z
Label3898:
ld a,(FF00+8C)
push af
ld a,1E
rst 10
ld hl,4000
ld a,b
add a,a
ld d,00
ld e,a
add hl,de
ldi a,(hl)
ld h,(hl)
ld l,a
call Label38BF
call Label385E
pop af
rst 10
ret

#org 226
Label226:
ld a,(C82B)
push af
ld a,(DA40)
push af
ld hl,C000
ld bc,1EF7
xor a,a
call Label1057
ld hl,DF00
ld bc,0100
xor a,a
call Label1057
ld hl,FF8A
ld c,20
xor a,a
call Label1060
pop af
ld (DA40),a
pop af
and a,CD
ld (C82B),a
ld a,03
rst 10
call Label10000
call Label1D11
ld a,E7
ld (C0A2),a
ld a,01
ld (C851),a
ld a,04
ld (DA43),a
ld hl,C859
xor a,a
ldd (hl),a
dec a
ld (hl),a
ret

#org FC1
LabelFC1:
ld c,80
ld b,0A
ld hl,0FCF
LabelFC8:
ldi a,(hl)
ld (FF00+c),a
inc c
dec b
jr nz,LabelFC8
ret

#org 1E4F
Label1E4F:
ld b,a
add a,a
add a,b
ld b,00
ld c,a
ld hl,1E86
add hl,bc
ld bc,DA3D
ldi a,(hl)
ld (FF00+47),a
ld (bc),a
inc bc
ldi a,(hl)
ld (FF00+48),a
ld (bc),a
inc bc
ld a,(hl)
ld (FF00+49),a
ld (bc),a
ret

#org 3737
Label3737:
ld a,(FF00+FF)
set 0,a
ei
ld a,0A
call Label388E
ld a,(FF00+00)
cp a,FF
jr nz,Label3761
call Label38F4
ld a,(FF00+00)
cp a,FF
jr nz,Label3761
ld a,09
call Label388E
ld hl,C82B
res 7,(hl)
ld a,CC
ld (DA40),a
di
ret

#org 1022
Label1022:
ld a,20
ld (FF00+00),a
ld a,(FF00+00)
ld a,(FF00+00)
cpl
and a,0F
swap a
ld b,a
ld a,30
ld (FF00+00),a
ld a,10
ld (FF00+00),a
ld a,(FF00+00)
ld a,(FF00+00)
ld a,(FF00+00)
ld a,(FF00+00)
ld a,(FF00+00)
ld a,(FF00+00)
cpl
and a,0F
or a,b
ld c,a
ld a,(FF00+8A)
xor a,c
and a,c
ld (FF00+8B),a
ld a,c
ld (FF00+8A),a
ld a,30
ld (FF00+00),a
ret

#org 1EC
Label1EC:
ld a,(FF00+8A)
and a,0F
cp a,0F
ret nz
ld a,(FF00+8E)
cp a,03
ret c
cp a,1A
ret z
ld a,5F
call Label3891
call LabelFF0
ld a,03
call Label1E4F
call Label10AB
ld a,(FF00+FF)
and a,FA
ld (FF00+FF),a
ei
call Label226
ld a,07
ld (C0A3),a
ld (FF00+FF),a
xor a,a
ld (FF00+90),a
ld a,02
ld (FF00+8E),a
jp LabelFFD
ld a,(C82B)
push af
ld a,(DA40)
push af
ld hl,C000
ld bc,1EF7
xor a,a
call Label1057
ld hl,DF00
ld bc,0100
xor a,a
call Label1057
ld hl,FF8A
ld c,20
xor a,a
call Label1060
pop af
ld (DA40),a
pop af
and a,CD
ld (C82B),a
ld a,03
rst 10
call Label10000
call Label1D11
ld a,E7
ld (C0A2),a
ld a,01
ld (C851),a
ld a,04
ld (DA43),a
ld hl,C859
xor a,a
ldd (hl),a
dec a
ld (hl),a
ret

#org 1A9
Label1A9:
ld a,(FF00+8E)
rst 08
ldi (hl),a
dec e
ldi (hl),a
dec e
ld c,34
sbc a,d
dec (hl)
xor a,b
jr z,Label1D9
dec e
ldi (hl),a
dec e
ldi (hl),a
dec e
ld d,a
ld (hl),22
dec e
ldi (hl),a
dec e
ldi (hl),a
dec e
cp a,C1
ldi (hl),a
dec e
ldi (hl),a
dec e
ldi (hl),a
dec e
cp a,l
ld b,DF
inc (hl)
sub a,d
ld b,22
dec e
ld l,h
ld h,82
dec (hl)
ld h,e
Label1D9:
ld (hl),22
dec e
nop
ld b,b
ldi (hl),a
dec e
ld a,c
ld (hl),22
dec e
ld (hl),c
inc (hl)
ldi (hl),a
dec e
ld a,(bc)
add hl,hl
ldi (hl),a
dec e
ld a,(FF00+8A)
and a,0F
cp a,0F
ret nz
ld a,(FF00+8E)
cp a,03
ret c
cp a,1A
ret z
ld a,5F
call Label3891
call LabelFF0
ld a,03
call Label1E4F
call Label10AB
ld a,(FF00+FF)
and a,FA
ld (FF00+FF),a
ei
call Label226
ld a,07
ld (C0A3),a
ld (FF00+FF),a
xor a,a
ld (FF00+90),a
ld a,02
ld (FF00+8E),a
jp LabelFFD
ld a,(C82B)
push af
ld a,(DA40)
push af
ld hl,C000
ld bc,1EF7
xor a,a
call Label1057
ld hl,DF00
ld bc,0100
xor a,a
call Label1057
ld hl,FF8A
ld c,20
xor a,a
call Label1060
pop af
ld (DA40),a
pop af
and a,CD
ld (C82B),a
ld a,03
rst 10
call Label10000
call Label1D11
ld a,E7
ld (C0A2),a
ld a,01
ld (C851),a
ld a,04
ld (DA43),a
ld hl,C859
xor a,a
ldd (hl),a
dec a
ld (hl),a
ret

#org 38BF
Label38BF:
ld a,(hl)
and a,07
ld b,a
ld c,00
push bc
Label38C6:
xor a,a
ld (FF00+c),a
ld a,30
ld (FF00+c),a
ld b,10
Label38CD:
ld e,08
ldi a,(hl)
ld d,a
Label38D1:
bit 0,d
ld a,10
jr nz,Label38D9
ld a,20
Label38D9:
ld (FF00+c),a
ld a,30
ld (FF00+c),a
rr d
dec e
jr nz,Label38D1
dec b
jr nz,Label38CD
ld a,20
ld (FF00+c),a
ld a,30
ld (FF00+c),a
pop bc
dec b
ret z
push bc
call Label385E
jr Label38C6
Label38F4:
ld a,20
ld (FF00+00),a
ld a,(FF00+00)
ld a,(FF00+00)
cpl
and a,0F
swap a
ld b,a
ld a,30
ld (FF00+00),a
ld a,10
ld (FF00+00),a
ld a,(FF00+00)
ld a,(FF00+00)
ld a,(FF00+00)
ld a,(FF00+00)
ld a,(FF00+00)
ld a,(FF00+00)
ld a,30
ld (FF00+00),a
ret

#org 385E
Label385E:
ld b,04
Label3860:
ld de,06D6
call Label3885
dec b
jr nz,Label3860
ret

#org 1057
Label1057:
ldi (hl),a
ld d,a
dec bc
ld a,b
or a,c
ld a,d
jr nz,Label1057
ret

#org 1060
Label1060:
ldi (hl),a
dec c
jr nz,Label1060
ret

#org 10000
Label10000:
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
inc a
nop
ld l,(hl)
nop
ld a,(hl)
nop
ld l,(hl)
nop
ld l,(hl)
nop
inc a
nop
nop
nop
nop
nop
inc e
nop
inc a
nop
inc e
nop
inc e
nop
inc e
nop
ld a,00
nop
nop
nop
nop
inc a
nop
ld l,(hl)
nop
ld l,(hl)
nop
inc e
nop
jr c,Label1003C
Label1003C:
ld a,(hl)
nop
nop
nop
nop
nop
inc a
nop
ld c,(hl)
nop
inc e
nop
ld c,(hl)
nop
ld l,(hl)
nop
inc a
nop
nop
nop
nop
nop
inc e
nop
inc a
nop
ld c,h
nop
ld e,h
nop
ld a,(hl)
nop
inc e
nop
nop
nop
nop
nop
ld a,h
nop
ld (hl),b
nop
ld a,h
nop
ld b,00
halt
nop
inc a
nop
nop
nop
nop
nop
inc a
nop
ld (hl),b
nop
ld a,h
nop
halt
nop
halt
nop
inc a
nop
nop
nop
nop
nop
ld a,(hl)
nop
ld l,(hl)
nop
ld c,00
inc e
nop
inc e
nop
inc e
nop
nop
nop
nop
nop
inc a
nop
ld l,(hl)
nop
inc a
nop
halt
nop
halt
nop
inc a
nop
nop
nop
nop
nop
inc a
nop
ld l,(hl)
nop
ld l,(hl)
nop
ld a,00
ld c,00
inc a
nop
nop
nop
nop
nop
inc a
nop
halt
nop
ld a,(hl)
nop
halt
nop
halt
nop
halt
nop
nop
nop
nop
nop
ld a,h
nop
halt
nop
ld a,h
nop
halt
nop
halt
nop
ld a,h
nop
nop
nop
nop
nop
inc a
nop
halt
nop
ld (hl),b
nop
halt
nop
halt
nop
inc a
nop
nop
nop
nop
nop
ld a,h
nop
halt
nop
halt
nop
halt
nop
halt
nop
ld a,h
nop
nop
nop
nop
nop
ld a,(hl)
nop
ld (hl),b
nop
ld a,(hl)
nop
ld (hl),b
nop
ld (hl),b
nop
ld a,(hl)
nop
nop
nop
nop
nop
ld a,(hl)
nop
ld (hl),b
nop
ld a,h
nop
ld (hl),b
nop
ld (hl),b
nop
ld (hl),b
nop
nop
nop
nop
nop
inc a
nop
halt
nop
ld (hl),b
nop
ld a,(hl)
nop
halt
nop
ld a,00
nop
nop
nop
nop
halt
nop
halt
nop
ld a,(hl)
nop
halt
nop
halt
nop
halt
nop
nop
nop
nop
nop
ld a,h
nop
jr c,Label10136
Label10136:
jr c,Label10138
Label10138:
jr c,Label1013A
Label1013A:
jr c,Label1013C
Label1013C:
ld a,h
nop
nop
nop
nop
nop
ld a,(hl)
nop
inc c
nop
inc c
nop
ld l,h
nop
ld a,h
nop
jr c,Label1014E
Label1014E:
nop
nop
nop
nop
ld h,(hl)
nop
ld l,h
nop
ld a,b
nop
ld a,h
nop
halt
nop
halt
nop
nop
nop
nop
nop
ld (hl),b
nop
ld (hl),b
nop
ld (hl),b
nop
ld (hl),b
nop
ld (hl),b
nop
ld a,(hl)
nop
nop
nop
nop
nop
ld h,d
nop
halt
nop
ld a,(hl)
nop
ld l,d
nop
ld l,d
nop
ld h,d
nop
nop
nop
nop
nop
ld (hl),d
nop
ld a,d
nop
ld a,(hl)
nop
ld a,(hl)
nop
halt
nop
halt
nop
nop
nop
nop
nop
inc a
nop
halt
nop
halt
nop
halt
nop
halt
nop
inc a
nop
nop
nop
nop
nop
ld a,h
nop
halt
nop
halt
nop
ld a,h
nop
ld (hl),b
nop
ld (hl),b
nop
nop
nop
nop
nop
inc a
nop
halt
nop
halt
nop
ld a,(hl)
nop
halt
nop
ld a,00
nop
nop
nop
nop
ld a,h
nop
halt
nop
halt
nop
ld a,h
nop
halt
nop
halt
nop
nop
nop
nop
nop
inc a
nop
ld (hl),b
nop
inc a
nop
ld c,00
ld l,(hl)
nop
inc a
nop
nop
nop
nop
nop
ld a,h
nop
jr c,Label101E6
Label101E6:
jr c,Label101E8
Label101E8:
jr c,Label101EA
Label101EA:
jr c,Label101EC
Label101EC:
jr c,Label101EE
Label101EE:
nop
nop
nop
nop
halt
nop
halt
nop
halt
nop
halt
nop
halt
nop
inc a
nop
nop
nop
nop
nop
halt
nop
halt
nop
halt
nop
halt
nop
inc a
nop
jr Label1020E
Label1020E:
nop
nop
nop
nop
ld h,d
nop
ld l,d
nop
ld l,d
nop
ld a,(hl)
nop
halt
nop
ld h,d
nop
nop
nop
nop
nop
ld l,(hl)
nop
inc l
nop
jr Label10228
Label10228:
jr c,Label1022A
Label1022A:
ld l,h
nop
ld l,(hl)
nop
nop
nop
nop
nop
halt
nop
halt
nop
halt
nop
inc a
nop
inc e
nop
inc e
nop
nop
nop
nop
nop
ld a,(hl)
nop
ld c,00
inc e
nop
jr c,Label1024A
Label1024A:
ld a,b
nop
ld a,(hl)
nop
nop
nop
nop
nop
nop
nop
nop
nop
inc a
nop
inc a
nop
nop
nop
nop
nop
nop
nop
nop
nop
inc a
nop
ld a,(hl)
nop
ld l,(hl)
nop
inc e
nop
nop
nop
jr Label1026E
Label1026E:
nop
nop
nop
nop
ld l,h
nop
ld l,h
nop
ld l,h
nop
ld c,b
nop
nop
nop
ld l,h
nop
nop
nop
nop
nop
ld (3C00),sp
nop
ld de,3E00
nop
ld d,l
nop
ld c,c
nop
ldd (hl),a
nop
nop
nop
nop
nop
ld b,d
nop
ld b,c
nop
ld b,c
nop
ld b,b
nop
ld c,b
nop
jr nc,Label102A0
Label102A0:
nop
nop
ld e,00
nop
nop
ld e,00
ld h,c
nop
ld bc,0200
nop
inc e
nop
nop
nop
inc e
nop
nop
nop
ld a,00
inc b
nop
ld (1400),sp
nop
daa
nop
nop
nop
ld de,3D00
nop
stop
ld a,00
ld d,c
nop
ld d,c
nop
ldd (hl),a
nop
nop
nop
ld (de),a
nop
ld a,c
nop
dec d
nop
dec h
nop
inc h
nop
ld b,h
nop
jr Label102E0
Label102E0:
nop
nop
stop
ld a,00
ld (3E00),sp
nop
inc b
nop
ld b,b
nop
inc a
nop
nop
nop
inc b
nop
ld (1000),sp
nop
jr nz,Label102FA
Label102FA:
stop
ld (0400),sp
nop
nop
nop
ld (bc),a
nop
ld c,a
nop
ld b,d
nop
ld b,d
nop
ld b,d
nop
ldi (hl),a
nop
inc b
nop
nop
nop
nop
nop
inc a
nop
ld (bc),a
nop
inc b
nop
nop
nop
jr nz,Label1031E
Label1031E:
ld e,00
nop
nop
ld (3E00),sp
nop
inc b
nop
ld (bc),a
nop
inc b
nop
jr nz,Label1032E
Label1032E:
ld e,00
nop
nop
jr nz,Label10334
Label10334:
jr nz,Label10336
Label10336:
jr nz,Label10338
Label10338:
jr nz,Label1033A
Label1033A:
jr nz,Label1033C
Label1033C:
ldi (hl),a
nop
inc e
nop
nop
nop
inc b
nop
ld a,a
nop
inc b
nop
inc e
nop
inc d
nop
inc c
nop
jr Label10350
Label10350:
nop
nop
ld (bc),a
nop
ldi (hl),a
nop
ld a,a
nop
ldi (hl),a
nop
ldi (hl),a
nop
jr nz,Label1035E
Label1035E:
ld e,00
nop
nop
ldi (hl),a
nop
ldi (hl),a
nop
inc b
nop
ld a,a
nop
stop
jr nz,Label1036E
Label1036E:
ld e,00
nop
nop
stop
ld a,(hl)
nop
stop
rla
nop
jr nz,Label1037C
Label1037C:
jr z,Label1037E
Label1037E:
ld b,a
nop
nop
nop
ld (3E00),sp
nop
stop
ld l,00
ld sp,0100
nop
ld c,00
nop
nop
nop
nop
ld e,00
ld h,c
nop
ld bc,0100
nop
ld (bc),a
nop
inc c
nop
nop
nop
nop
nop
ccf
nop
ld b,00
ld (1000),sp
nop
stop
ld c,00
nop
nop
jr nz,Label103B4
Label103B4:
ld hl,1600
nop
jr Label103BA
Label103BA:
jr nz,Label103BC
Label103BC:
jr nz,Label103BE
Label103BE:
rra
nop
nop
nop
stop
ld a,e
nop
stop
ldi (hl),a
nop
ld c,(hl)
nop
inc de
nop
inc c
nop
nop
nop
nop
nop
ld c,a
nop
ld b,b
nop
ld b,b
nop
ld b,b
nop
ld c,b
nop
daa
nop
nop
nop
inc b
nop
inc h
nop
ld a,00
dec (hl)
nop
ld e,e
nop
ld d,l
nop
ld h,00
nop
nop
nop
nop
ld h,00
ld l,c
nop
ld sp,2300
nop
ld h,l
nop
ldi (hl),a
nop
nop
nop
nop
nop
ld e,00
add hl,hl
nop
ld c,c
nop
ld d,c
nop
ld d,c
nop
ldi (hl),a
nop
nop
nop
ld (bc),a
nop
ld c,a
nop
ld b,d
nop
ld b,d
nop
ld c,(hl)
nop
ld d,e
nop
ld l,00
nop
nop
stop
ld (hl),d
nop
inc hl
nop
ld b,d
nop
ld b,d
nop
ld b,h
nop
jr c,Label10430
Label10430:
nop
nop
jr Label10434
Label10434:
inc b
nop
nop
nop
jr z,Label1043A
Label1043A:
ld b,l
nop
ld b,l
nop
jr Label10440
Label10440:
nop
nop
nop
nop
nop
nop
jr Label10448
Label10448:
inc h
nop
ld b,d
nop
ld bc,0000
nop
nop
nop
ld c,a
nop
ld b,d
nop
ld c,a
nop
ld b,d
nop
ld c,(hl)
nop
ld d,e
nop
inc l
nop
nop
nop
ld a,00
inc b
nop
ld a,00
inc b
nop
inc a
nop
ld b,(hl)
nop
jr c,Label10470
Label10470:
nop
nop
inc a
nop
inc b
nop
ld a,(bc)
nop
ccf
nop
ld d,d
nop
ld d,d
nop
inc h
nop
nop
nop
stop
ld a,e
nop
ld de,3000
nop
ld d,b
nop
ld hl,1E00
nop
nop
nop
inc b
nop
inc h
nop
ld a,00
dec (hl)
nop
ld e,c
nop
ld d,c
nop
ld h,00
nop
nop
stop
inc a
nop
stop
inc a
nop
ld de,1100
nop
ld c,00
nop
nop
inc h
nop
ld l,00
ld (hl),l
nop
ld hl,1200
nop
stop
ld (0000),sp
nop
inc b
nop
ld c,(hl)
nop
ld d,l
nop
ld h,l
nop
ld c,l
nop
ld b,00
ld (0000),sp
nop
ld (0E00),sp
nop
ld (0800),sp
nop
inc a
nop
ld c,d
nop
jr nc,Label104E0
Label104E0:
nop
nop
inc e
nop
ld (bc),a
nop
stop
ld l,00
ld sp,0100
nop
ld c,00
nop
nop
ldi (hl),a
nop
ldi (hl),a
nop
ldi (hl),a
nop
ldi a,(hl)
nop
ld (de),a
nop
inc b
nop
jr Label10500
Label10500:
nop
nop
ld a,00
inc b
nop
inc c
nop
ldd (hl),a
nop
ld c,c
nop
dec d
nop
ld c,00
nop
nop
nop
nop
ld h,00
ld l,d
nop
ldd (hl),a
nop
ldi (hl),a
nop
ld h,d
nop
ld hl,0000
nop
ld a,00
inc b
nop
ld (1E00),sp
nop
ld h,c
nop
ld bc,1E00
nop
nop
nop
nop
nop
ld h,00
ld l,c
nop
ld sp,2100
nop
ld h,c
nop
ldi (hl),a
nop
nop
nop
stop
ld a,h
nop
jr nz,Label10548
Label10548:
halt
nop
jr Label1054C
Label1054C:
jr nz,Label1054E
Label1054E:
ld a,00
nop
nop
ld (0800),sp
nop
stop
stop
add hl,sp
nop
add hl,hl
nop
ld b,(hl)
nop
nop
nop
nop
nop
nop
nop
jr c,Label10568
Label10568:
inc d
nop
jr c,Label1056C
Label1056C:
ld (hl),h
nop
jr z,Label10570
Label10570:
nop
nop
nop
nop
nop
nop
nop
nop
ld c,b
nop
ld b,h
nop
ld d,b
nop
jr nz,Label10580
Label10580:
nop
nop
nop
nop
nop
nop
stop
jr Label1058A
Label1058A:
inc h
nop
inc b
nop
jr Label10590
Label10590:
nop
nop
nop
nop
nop
nop
stop
jr c,Label1059A
Label1059A:
ld (1000),sp
nop
inc l
nop
nop
nop
nop
nop
nop
nop
stop
inc (hl)
nop
stop
inc a
nop
inc (hl)
nop
nop
nop
nop
nop
nop
nop
nop
nop
jr Label105BA
Label105BA:
ld h,h
nop
inc b
nop
jr Label105C0
Label105C0:
nop
nop
nop
nop
nop
nop
ld d,b
nop
jr c,Label105CA
Label105CA:
ld h,h
nop
inc h
nop
stop
nop
nop
nop
nop
nop
nop
stop
ld e,b
nop
ld h,h
nop
ld e,b
nop
stop
nop
nop
nop
nop
nop
nop
stop
jr Label105EA
Label105EA:
jr nc,Label105EC
Label105EC:
ld e,b
nop
jr nz,Label105F0
Label105F0:
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
ld a,(bc)
nop
ld a,(bc)
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
ld c,00
ld a,(bc)
nop
ld c,00
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
ld (FF00+00),a
and a,b
nop
ld (FF00+00),a
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
add a,b
nop
add a,b
nop
ld b,b
nop
nop
nop
nop
nop
stop
jr Label10636
Label10636:
;unknown opcode FC

#org 1D11
Label1D11:
call Label1D07
ld hl,DA71
ld de,DA61
ld a,e
ldi (hl),a
ld a,d
ldi (hl),a
ld a,e
ldi (hl),a
ld (hl),d
ret

#org 3761
Label3761:
ld hl,C82B
set 7,(hl)
ld a,9C
ld (DA40),a
ld a,2D
call Label3857
ld a,1F
call Label3857
di
call Label386A
ld a,E4
ld (FF00+47),a
ld hl,9800
ld de,000C
ld a,80
ld b,0D
Label3787:
ld c,14
Label3789:
ldi (hl),a
inc a
dec c
jr nz,Label3789
add hl,de
dec b
jr nz,Label3787
ld a,1E
rst 10
ld hl,5212
ld de,8800
ld bc,1000
call Label257F
ld a,81
ld (FF00+40),a
ld a,27
call Label3857
ei
di
call Label386A
ld hl,5D9C
ld de,8800
ld bc,06E0
call Label257F
ld a,81
ld (FF00+40),a
ld a,2A
call Label3857
ei
di
call Label386A
ld hl,6145
ld de,8800
ld bc,0860
call Label257F
ld a,81
ld (FF00+40),a
ld a,28
call Label3857
ei
di
call Label386A
ld hl,4710
ld de,8800
ld bc,1000
call Label257F
ld a,81
ld (FF00+40),a
ld a,10
call Label3857
ei
di
call Label386A
ld hl,4C92
ld de,8800
ld bc,0FD2
call Label257F
ld a,81
ld (FF00+40),a
ld a,11
call Label3857
ei
di
call Label386A
ld a,0C
rst 10
ld hl,5DDD
ld de,8800
ld bc,0680
call Label1065
ld a,81
ld (FF00+40),a
ld a,20
call Label3857
ei
di
call Label386A
ld hl,6438
ld de,8800
ld bc,0BCA
call Label1065
ld a,81
ld (FF00+40),a
ld a,20
call Label3857
ei
ld a,05
call Label3891
ld a,09
call Label3891
jp Label391B
Label3857:
call Label3891
ld b,08
jr Label3860
ld b,04
ld de,06D6
call Label3885
dec b
jr nz,Label3860
ret

#org FF0
LabelFF0:
di
ld a,(FF00+FF)
ld (C0A3),a
and a,FE
ld (FF00+FF),a
ei
jr Label1010
LabelFFD:
ld a,(C0A3)
ld (FF00+FF),a
ld a,(C0A2)
ld (FF00+40),a
ret

#org 10AB
Label10AB:
ld hl,9800
ld bc,0800
jr Label10BE
ld hl,9800
jr Label10BB
ld hl,9C00
Label10BB:
ld bc,0400
Label10BE:
xor a,a
jp Label1057
ld hl,C000
ld c,A0
xor a,a
call Label1060
ld hl,C200
ld bc,0400
xor a,a
jp Label1057
ld a,(FF00+8C)
push af
ld a,1D
rst 10
jr Label10E3
ld a,(FF00+8C)
push af
ld a,0B
rst 10
Label10E3:
xor a,a
ld (C701),a
ld de,C000
ld hl,C200
ld b,20
Label10EF:
ldi a,(hl)
or a,a
call nz, Label112C
ld a,l
add a,1F
ld l,a
ld a,h
adc a,00
ld h,a
dec b
jr nz,Label10EF
ld h,d
ld l,e
ld b,A0
Label1103:
ld a,l
cp a,b
jr z,Label110E
xor a,a
ldi (hl),a
ldi (hl),a
ldi (hl),a
ldi (hl),a
jr Label1103
Label110E:
xor a,a
ld (C701),a
pop af
rst 10
ret

#org 8
Label8:
jp LabelFD9
rst 38
rst 38
rst 38
rst 38
rst 38
ld (FF00+8C),a
ld (2000),a
ret

#org 3885
Label3885:
nop
nop
nop
dec de
ld a,d
or a,e
jr nz,Label3885
ret

#org 1D07
Label1D07:
ld hl,DA5F
ld de,DA61
ld a,e
ldi (hl),a
ld (hl),d
ret

#org 386A
Label386A:
ld a,(FF00+FF)
ld (C0A3),a
res 0,a
ld (FF00+FF),a
Label3873:
ld a,(FF00+44)
cp a,91
jr c,Label3873
ld a,(FF00+40)
and a,7F
ld (FF00+40),a
ld a,(C0A3)
ld (FF00+FF),a
ret

#org 257F
Label257F:
push hl
ld hl,C809
ld a,c
ldi (hl),a
ld a,b
ldi (hl),a
ld a,e
ldi (hl),a
ld (hl),d
ld de,0000
pop hl
Label258E:
ldi a,(hl)
ld b,a
ld c,08
Label2592:
push bc
bit 0,b
jr z,Label25A9
ldi a,(hl)
call Label25E7
ld a,(C809)
cp a,e
jr nz,Label25DD
ld a,(C80A)
cp a,d
jr nz,Label25DD
jr Label25E5
Label25A9:
ldi a,(hl)
ld c,a
ldi a,(hl)
push hl
ld b,a
and a,F0
swap a
ld h,a
ld a,e
sub a,c
ld l,a
ld a,d
sbc a,h
ld h,a
ld a,b
and a,0F
add a,03
ld c,a
ld a,(C80B)
add a,l
ld l,a
ld a,(C80C)
adc a,h
ld h,a
Label25C9:
ldi a,(hl)
call Label25E7
dec c
jr nz,Label25C9
pop hl
ld a,(C809)
cp a,e
jr nz,Label25DD
ld a,(C80A)
cp a,d
jr z,Label25E5
Label25DD:
pop bc
slr b
dec c
jr nz,Label2592
jr Label258E
Label25E5:
pop bc
ret

#org 1065
Label1065:
ldi a,(hl)
ld (de),a
inc de
dec bc
ld a,b
or a,c
jr nz,Label1065
ret

#org 391B
Label391B:
ld c,08
ld a,68
Label391F:
push af
push bc
call Label3891
pop bc
pop af
inc a
dec c
jr nz,Label391F
ret

#org 1010
Label1010:
ld a,(FF00+44)
cp a,91
jr c,Label1010
ld a,(FF00+40)
and a,7F
ld (FF00+40),a
ld a,(C0A3)
ld (FF00+FF),a
ret

#org 112C
Label112C:
push bc
push hl
ldi a,(hl)
ld (C700),a
ld c,a
ldi a,(hl)
add a,a
ld b,a
push hl
ldi a,(hl)
ld d,a
ldi a,(hl)
ld e,a
inc hl
ldd a,(hl)
add a,e
ld e,a
ld a,d
adc a,(hl)
pop hl
ldi (hl),a
ld (C702),a
ld a,e
ldi (hl),a
inc hl
inc hl
push hl
ldi a,(hl)
ld d,a
ldi a,(hl)
ld e,a
push hl
inc hl
ldi a,(hl)
inc (hl)
add a,e
ld e,a
ld a,d
pop hl
adc a,(hl)
pop hl
ldi (hl),a
ld (C703),a
ld (hl),e
Label115E:
ld hl,4000
ld d,00
ld e,c
add hl,de
add hl,de
ldi a,(hl)
ld h,(hl)
ld l,a
ld e,b
add hl,de
ldi a,(hl)
ld e,a
ld a,(hl)
cp a,F0
jr z,Label117F
cp a,F1
jr nz,Label118A
ld a,e
pop de
push de
inc de
ld (de),a
add a,a
ld b,a
jr Label115E
Label117F:
ld a,e
pop de
push de
ld (de),a
inc de
ld c,a
xor a,a
ld b,a
ld (de),a
jr Label115E
Label118A:
ld d,a
push de
ld hl,C600
ld d,00
ld a,(C700)
ld e,a
add hl,de
ld a,(hl)
ld (C700),a
pop hl
ld d,C0
ld a,(C701)
ld e,a
ldi a,(hl)
ld b,a
Label11A3:
ld a,b
or a,a
jr z,Label11E1
dec b
ld a,e
cp a,A0
jr nz,Label11BD
ld a,(FF00+95)
rrca
jr nc,Label11E1
push hl
ld hl,sp+06
ld a,1A
ldi (hl),a
ld (hl),11
pop hl
ld e,50
Label11BD:
ld a,(C707)
ld c,a
ld a,(C702)
add a,(hl)
sub a,c
ld (de),a
inc hl
inc de
ld a,(C708)
ld c,a
ld a,(C703)
add a,(hl)
sub a,c
ld (de),a
inc hl
inc de
ld a,(C700)
add a,(hl)
inc hl
ld (de),a
inc de
ldi a,(hl)
ld (de),a
inc de
jr Label11A3
Label11E1:
ld a,e
ld (C701),a
pop hl
pop bc
ret

#org FD9
LabelFD9:
pop hl
push de
ld e,a
ld d,00
add hl,de
add hl,de
pop de
ldi a,(hl)
ld h,(hl)
ld l,a
jp hl
ld (FF00+8C),a
ld (2000),a
ret

#org 38
Label38:
ret

#org 25E7
Label25E7:
ld b,a
push hl
ld hl,C80B
ldi a,(hl)
ld h,(hl)
ld l,a
add hl,de
ld (hl),b
inc de
pop hl
retp bc
slr b
dec c
jr nz,Label2592
jr Label258E
Label25E5:
pop bc
ret

#org 1065
Label1065:
ldi a,(hl)
ld (de),a
inc de
dec bc
ld a,b
or a,c
jr nz,Label1065
ret

#org 391B
Label391B:
ld c,08
ld a,68
Label391F:
push af
push bc
call Label3891
pop bc
pop af
inc a
dec c
jr nz,Label391F
ret

#org 1010
Label1010:
ld a,(FF00+44)
cp a,91
jr c,Label1010
ld a,(FF00+40)
and a,7F
ld (FF00+40),a
ld a,(C0A3)
ld (FF00+FF),a
ret

#org 112C
Label112C:
push bc
push hl
ldi a,(hl)
ld (C700),a
ld c,a
ldi a,(hl)
add a,a
ld b,a
push hl
ldi a,(hl)
ld d,a
ldi a,(hl)
ld e,a
inc hl
ldd a,(hl)
add a,e
ld e,a
ld a,d
adc a,(hl)
pop hl
ldi (hl),a
ld (C702),a
ld a,e
ldi (hl),a
inc hl
inc hl
push hl
ldi a,(hl)
ld d,a
ldi a,(hl)
ld e,a
push hl
inc hl
ldi a,(hl)
inc (hl)
add a,e
ld e,a
ld a,d
pop hl
adc a,(hl)
pop hl
ldi (hl),a
ld (C703),a
ld (hl),e
Label115E:
ld hl,4000
ld d,00
ld e,c
add hl,de
add hl,de
ldi a,(hl)
ld h,(hl)
ld l,a
ld e,b
add hl,de
ldi a,(hl)
ld e,a
ld a,(hl)
cp a,F0
jr z,Label117F
cp a,F1
jr nz,Label118A
ld a,e
pop de
push de
inc de
ld (de),a
add a,a
ld b,a
jr Label115E
Label117F:
ld a,e
pop de
push de
ld (de),a
inc de
ld c,a
xor a,a
ld b,a
ld (de),a
jr Label115E
Label118A:
ld d,a
push de
ld hl,C600
ld d,00
ld a,(C700)
ld e,a
add hl,de
ld a,(hl)
ld (C700),a
pop hl
ld d,C0
ld a,(C701)
ld e,a
ldi a,(hl)
ld b,a
Label11A3:
ld a,b
or a,a
jr z,Label11E1
dec b
ld a,e
cp a,A0
jr nz,Label11BD
ld a,(FF00+95)
rrca
jr nc,Label11E1
push hl
ld hl,sp+06
ld a,1A
ldi (hl),a
ld (hl),11
pop hl
ld e,50
Label11BD:
ld a,(C707)
ld c,a
ld a,(C702)
add a,(hl)
sub a,c
ld (de),a
inc hl
inc de
ld a,(C708)
ld c,a
ld a,(C703)
add a,(hl)
sub a,c
ld (de),a
inc hl
inc de
ld a,(C700)
add a,(hl)
inc hl
ld (de),a
inc de
ldi a,(hl)
ld (de),a
inc de
jr Label11A3
Label11E1:
ld a,e
ld (C701),a
pop hl
pop bc
ret

#org 25E7
Label25E7:
ld b,a
push hl
ld hl,C80B
ldi a,(hl)
ld h,(hl)
ld l,a
add hl,de
ld (hl),b
inc de
pop hl
ret